import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Edward Rodriguez
 * Date: 11/22/16
 * 
 * Program that reads one or more arithmetic expressions written in infix notation from a .txt file
 * then converts and prints the equivalent arithmetic expression into postfix notation.
 *
 */
public class Project2 {
	
	public static String FILE = "project2.txt";
	public static String SPACE = "\\s+";
	public static String REGEX = "(?<=[-�+*/()])|(?=[-�+*/()])";
	
	/**
	 * Main method opens the file "project2.txt" then reads 1 line at a time
	 * until the end of the file is reached. Each line (arithmetic expression) is split 
	 * into substrings around matches of the regular expression (operators & parentheses). The substrings 
	 * are then stored in an array of type String and passed to the TokenStack class to 
	 * convert to postfix.
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		
		FileInputStream fstream = new FileInputStream(FILE);
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(fstream))){
			
			String [] lineArray ={};
			String line;
	
			while ((line = reader.readLine()) != null){
				if (!line.isEmpty()) {
					System.out.println("Infix expression: " + line);
					line = line.replaceAll(SPACE,""); 
					lineArray = line.split(REGEX);
					TokenStack t1 = new TokenStack(lineArray);
					t1.convertToPostfix();
				}
				System.out.print("\n");
			}	
		} catch (FileNotFoundException e) {
			e.printStackTrace();	
		  }	
	}
}
