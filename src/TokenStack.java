/**
 * @author Edward Rodriguez
 * TokenStack processes each token from left to right in the arithmetic expression (String array)
 * and performs the appropriate action depending on the token (push/pop/print)
 */
public class TokenStack {
	
	public static String LEFTPARENTHESIS = "(";
	public static String RIGHTPARENTHESIS = ")";
	public static String REGEX = "([-�+*/()])";
	public static String REGEXLOWPRECEDENCE = "([-�+])";
	public static String REGEXHIGHPRECEDENCE = "([*/])";
	public static String REGEXNOPARENTHESIS = "([-�+*/])";
	public static String SPACE = " ";
	
	//stack to store operators/parentheses
	private DynamicArrayStack<String> stack;
	private String[] arrayToProcess;
	
	
	//Default constructor initializing stack
	 public TokenStack(){
		stack = new DynamicArrayStack<>();
	}
	
	/**
	 * Constructor initializing stack & length
	 * also clones string to process (arithmetic expression)
	 * @param s string to  process
	 */
	public TokenStack(String[] s){
		stack = new DynamicArrayStack<>(s.length);
		arrayToProcess = s.clone();	
	}
	
	public DynamicArrayStack<String> getStack() {
		return stack;
	}

	public void setStack(DynamicArrayStack<String> stack) {
		this.stack = stack;
	}
	
	/**
	 * loops through each element of the string array to be processed
	 * and passes it to another method to determine the appropriate
	 * action for that element/token
	 * After the last token in the arithmetic expression is read and processed, 
	 * the remaining operators on the stack are popped and printed
	 */
	public void convertToPostfix(){
		System.out.print("Postfix: ");
		for (int i = 0; i < arrayToProcess.length; i++) {
			processToken(arrayToProcess[i]);
		}
		while (!stack.isEmpty()){
			System.out.print(stack.pop() + SPACE);
		}
		System.out.print("\n");
	}

	/**
	 * processes substring and performs the appropriate action 
	 * depending on the token (push/pop/print)
	 * @param s substring to be processed
	 */
	private void processToken (String s){
		if(stack.isEmpty() && s.matches(REGEX)) stack.push(s);
		
		else if (!stack.isEmpty() && s.matches(REGEX)){
			while(!stack.isEmpty()){
				if (s.matches(REGEXLOWPRECEDENCE) && stack.top().matches(REGEXNOPARENTHESIS)){
					System.out.print(stack.pop() + SPACE);
				}
				else if (s.matches(REGEXHIGHPRECEDENCE) && stack.top().matches(REGEXHIGHPRECEDENCE)){
					System.out.print(stack.pop() + SPACE);
				}
				else if (s.matches(REGEXHIGHPRECEDENCE) && stack.top().matches(REGEXLOWPRECEDENCE)){
					break;
				}
				else if (s.equals(LEFTPARENTHESIS)){
					break;
				}
				else if (s.equals(RIGHTPARENTHESIS)){
					while (!stack.top().equals(LEFTPARENTHESIS)){
						System.out.print(stack.pop() + SPACE);
					}
					if (stack.top().equals(LEFTPARENTHESIS)){
						stack.pop();
						break;
					}
				}
				else break;
			}
			if (!s.equals(RIGHTPARENTHESIS)) stack.push(s);	
		}
		else System.out.print(s + SPACE);	
	}
}
